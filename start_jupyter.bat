::##########
:: This batch file activates an anaconda environment and starts jupyter notebook
::
:: You might adapt the two variables:
:: conda_root: path to condabin folder (to find path, open anacond promt run "where python", add "condabin" to the path
:: environment: name of anaconda environment
::##########

@echo off
set conda_root=C:\Users\Peterdietiker\Anaconda3\condabin

set environment=mobidiag

echo activating environment %environment%
call %conda_root%\activate.bat %environment%
echo starting jupyter
call jupyter notebook

library('rmarkdown')
input_file <- "test file"
output <- "report file"
title <- "test title"
setup <- "test setup"

rmarkdown::render('Mobidiag_test_evaluation.Rmd',output_file=output,params = list(file = input_file, title = title, setup=setup),encoding="UTF-8")

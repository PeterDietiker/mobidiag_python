## TC2.9: Setup stability

Relative standard deviation of buffer signal over 90 minutes in percent.

```{r}
get_data <- function(dye, chamber = 1){
  raw_data[test == "TC2.9" & LED == get_LED(dye) ,eval(parse(text = get_channel(dye, chamber = chamber)))]
}
parameters <- data.frame(chamber = factor(c(1,2)))
results <- data.frame()
for (row in 1:nrow(parameters)){
  result <- data.frame(sapply(Filter_list,get_data, chamber = parameters[row, "chamber"]))
  result["chamber"] = parameters[row, "chamber"]
  result["index"] = seq.int(nrow(result))
  results <- rbind(results,result)
}

results = setDT(results)
change <- results[,lapply(.SD,cv),by = chamber]
#calculate percents
(change <- change[,(Filter_list) :=lapply(.SD,function(x) x*100),.SDcols=Filter_list])


```

```{r}
change[,(Filter_list),with=FALSE]
```

### Plot of time series

```{r}
relative_deviations <- copy(results)
relative_deviations[,(Filter_list) :=lapply(.SD,function(x) x/mean(x)), by = chamber]
deviations <- copy(results)
deviations[,(Filter_list) :=lapply(.SD,function(x) x-mean(x)), by = chamber]
```


```{r fig.width=7, fig.height=8}

results_long <- melt(results,measure = .SD,id = c("index","chamber"),variable.name = "Dye")
ggplot(results_long,aes(x=index,y=value,group=chamber,col=chamber))+geom_line()+geom_point(stat="summary",fun.y=sum)+facet_grid(Dye~., scales = "free_y")+xlab("Index")
```

### Plot of absolute deviations from mean

```{r fig.width=7, fig.height=8}
results_long <- melt(deviations,measure = .SD,id = c("index", "chamber"),variable.name = "Dye")
ggplot(results_long,aes(x=index,y=value,group=chamber,col=chamber))+geom_line()+geom_point(stat="summary",fun.y=sum)+
facet_grid(Dye~., scales = "free_y")+xlab("Index")
```

### Plot of relative deviations from mean

```{r fig.width=7, fig.height=8}
results_long <- melt(relative_deviations,measure = .SD,id = c("index", "chamber"),variable.name = "Dye")
ggplot(results_long,aes(x=index,y=value,group=chamber,col=chamber))+geom_line()+geom_point(stat="summary",fun.y=sum)+
facet_grid(Dye~., scales = "free_y")+xlab("Index")
```

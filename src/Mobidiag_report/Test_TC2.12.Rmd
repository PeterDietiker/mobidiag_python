## TC2.12: signal at max. concentration


DAC values for a 6-Plex mixture and SYBRGreen at maximum concentration

```{r}
get_data <- function(dye,conc, chamber = 1, data = raw_data){
  data[test == "TC2.12" & LED == get_LED(dye)  & eval(parse(text = get_LED(dye))) == conc,eval(parse(text = get_channel(dye,chamber = chamber,type = "raw")))]
}

  
results <- data.frame()
parameters = data.frame(conc = c(0,250,0),SYBR_conc = c(0,0,2500))
parameters = rbind(parameters,parameters)
parameters["chamber"] = rep(c(1,2),each=3)
row <- 1
for (row in 1:nrow(parameters)){
  result <- colMeans(data.frame(sapply(Filter_list[Filter_list != "SYBRGreen"],get_data,conc=parameters[row,"conc"],chamber = parameters[row,"chamber"], data = raw_data[SYBRGreen == parameters[row,"SYBR_conc"],])))
  results <- rbind(results,result)
}
colnames(results) <- Filter_list[Filter_list != "SYBRGreen"]

get_data <- function(dye,conc, chamber = 1, data = raw_data){
  data[test == "TC2.12" & LED == get_LED(dye)  & SYBRGreen == conc,eval(parse(text = get_channel(dye,chamber = chamber)))]
}

results_SYBR <- data.frame()
parameters = data.frame(conc = c(0,0,2500), FAM_conc = c(0,250,0))
parameters = rbind(parameters,parameters)
parameters["chamber"] = rep(c(1,2),each=3)

for (row in 1:nrow(parameters)){
  result <- colMeans(data.frame(sapply("SYBRGreen",get_data,conc=parameters[row,"conc"],chamber = parameters[row,"chamber"], data = raw_data[FAM == parameters[row,"FAM_conc"],])))
  results_SYBR <- rbind(results_SYBR,result)
}
colnames(results_SYBR) <- c("SYBRGreen")

results <- cbind(results,results_SYBR)
parameters = data.frame(Sample = c("Blank","6-plex","SYBRGreen"),Chamber = rep(c(1,2),each=3))
results <- setDT(cbind(parameters,results))
results
```

```{r}
results_long <- melt(results,measure = .SD,id = c("Sample","Chamber"),variable.name = "Dye")
results_long$Chamber <- as.factor(results_long$Chamber)
ggplot(results_long,aes(x=Dye,y=value,group=Chamber,col=Chamber))+geom_point()+facet_grid(Sample~., scales = "free_y")+xlab("Channel")+ylab("DAC value")
```




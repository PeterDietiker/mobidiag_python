---
output: 
  html_document: 
    df_print: kable
    toc: true
    toc_float: true
params:
  file: data-file
  title: "My Title!"
  date: !r Sys.Date()
  setup: setup
  
  
title: "`r params$title`"
date : "`r params$date`"
setup: "`r params$setup`"
---



```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
#knitr::opts_chunk$set(echo = TRUE)
library(dplyr)
library(ggplot2)
library(tidyr)
library(gridExtra)
library(kableExtra)
#library(multcomp)
library(data.table)
#library(hash)
#library(kableExtra)
options(contrasts = c("contr.sum","contr.sum"))
```





```{r include=FALSE}
load_data <- function(fn, conc_factor = TRUE){
  df <- read.csv(fn,sep=",",header=TRUE)
  df$test <- as.factor(df$test)
  #df$TEC <- as.factor(df$TEC)
  df$TEC1 <- as.factor(df$TEC1)
  df$TEC2 <- as.factor(df$TEC2)
  return(df)
}
```



```{r eval = params$setup=="V2"}
#Filter_list <- c("FAM","HEX","ATTO725","ROX","Q670","Q705","SYBRGreen")
########
#The following section has to be adapted according to the used lab model version
########

#
##For V2 (from 2021 on)
#
Dye_list = c("FAM","HEX","TAMRA","ROX","TYE665","Q705","SYBRGreen")
Filter_list <- c("FAM","HEX_a","HEX_b","TAMRA","ROX","TYE665","Q705_b","SYBRGreen")
##limit analysis to some Dyes
##Filter_list <- c("FAM","Q705_b")
Channel_list <- c("FAM","HEX_a","HEX_b","TAMRA","ROX","TYE665","Q705_b","SYBRGreen")
wavelengths = c(465,525,570,625,685,720,10000)
names(wavelengths) <- c("FAM","HEX","TAMRA","ROX","TYE665","Q705","SYBRGreen")
###########

####ONLY FOR MEASUREMENTS without old dyes
Dye_list = c("FAM","HEX","ROX","Q705","SYBRGreen")
Filter_list <- c("FAM","HEX_a","HEX_b","ROX","Q705_b","SYBRGreen")
Channel_list <- c("FAM","HEX_a","HEX_b","ROX","Q705_b","SYBRGreen")

```


```{r eval = params$setup=="V2.5"}
#Filter_list <- c("FAM","HEX","ATTO725","ROX","Q670","Q705","SYBRGreen")
########
#The following section has to be adapted according to the used lab model version
########

#
Dye_list = c("FAM","HEX","ROX","Q670","Q705","ALEXA750","SYBRGreen")
#Filter_list <- c("FAM","Q705_a","ROX","Q705_b","HEX_a","ALEXA750","Q670","HEX_b","SYBRGreen")
Filter_list <-  c("FAM","HEX_a","HEX_b","ROX","Q670","Q705_b","ALEXA750","SYBRGreen")
#Filter_list <- c("FAM","ROX","Q705_b","HEX_a","ALEXA750","Q670","HEX_b","SYBRGreen")
##limit analysis to some Dyes
##Filter_list <- c("FAM","Q705_b")
Channel_list <- c("FAM","ROX","Q705_b","HEX_a","ALEXA750","Q670","HEX_b","SYBRGreen")
wavelengths = c(465,525,570,625,685,720,10000)
names(wavelengths) <- c("FAM","HEX","ROX","Q670","Q705","ALEXA750","SYBRGreen")
###########

```

```{r eval = params$setup=="V3"}
#Filter_list <- c("FAM","HEX","ATTO725","ROX","Q670","Q705","SYBRGreen")
########
#The following section has to be adapted according to the used lab model version
########

#
##For V2 (from 2021 on)
#
Dye_list = c("FAM","HEX","ROX","Q670","Q705","ALEXA750","SYBRGreen")
Filter_list <- c("FAM","HEX","ROX","Q670","Q705","ALEXA750","SYBRGreen")
##limit analysis to some Dyes
##Filter_list <- c("FAM","Q705_b")
Channel_list <- c("FAM","HEX","ROX","Q670","Q705","ALEXA750","SYBRGreen")
wavelengths = c(465,525,570,625,685,720,10000)
names(wavelengths) <- c("FAM","HEX","ROX","Q670","Q705","ALEXA750","SYBRGreen")
###########

```

```{r}
get_LED <- function(channel){
  if (channel == "SYBRGreen"){
    LED <- "SYBRGreen"
  }
  else
  {
    LED <- strsplit(channel,"_")[[1]][1]
  }
  return(LED)
}

get_channel <- function(dye,quadrant = "a", chamber = 1,type = "avg"){
  if (dye == "SYBRGreen"){
    channel <- paste("FAM",toString(chamber),type,sep="_")
  }
  else
  {
  
    channel <- paste(dye,toString(chamber),type,sep="_")
  }
    
  return(channel)
}
all_channel_list <- unname(c(sapply(Filter_list,get_channel),sapply(Filter_list,get_channel,chamber=2)))

```

```{r}
cv <- function(data){
  sd(data)/mean(data)

}
```


<!-- Daten werden geladen -->
```{r load_data}
#path = "C:\\Daten\\Mobidiag\\Daten\\Test\\test_unit.csv"
#path = "V:\\Projects\\71040-02 Mobidiag_Novodiag2_Phase1\\300_Gate_Review_&_Test\\320_Test\\TC2.6-TC2.16\\200604\\200604_converted.csv"
#path = "V:\\Projects\\71040-02 Mobidiag_Novodiag2_Phase1\\300_Gate_Review_&_Test\\320_Test\\TC2.6\\200603\\TC2.6_converted.csv"
#path = "V:\\Projects\\71040-02 #Mobidiag_Novodiag2_Phase1\\300_Gate_Review_&_Test\\320_Test\\TC3.6\\200604\\TC3.6_converted.csv"
#path = "V:\\Projects\\71040-02 Mobidiag_Novodiag2_Phase1\\300_Gate_Review_&_Test\\320_Test\\TC4.3 #LOD\\200603\\TC4.3_converted.csv"
#path = "V:\\Projects\\71040-02 #Mobidiag_Novodiag2_Phase1\\300_Gate_Review_&_Test\\320_Test\\TC5.3\\200602\\TC5.3_converted.csv"
#path = "V:\\Projects\\71040-03 Mobidiag Lab Model Upgrade\\300_Gate_Review_&_Test\\320_Test\\TC5.3\\200603\\TC5.3_converted.csv"
#path = "C:\\Daten\\Python\\Mobidiag\\Mobidiag_python\\V3.csv"
#path = "P:\\Mikolaj Glinski\\2021-09-21 Mobidiag LOD testing\\V2\\test_converted.csv"
path = params$file
raw_data <- setDT(load_data(path))

```
```{r}
#df <- read.csv(path,sep=",",header=TRUE)
```


```{r TC2.6, child='Test_TC2.6.Rmd', eval = "TC2.6" %in% raw_data$test}

```

```{r, child='Test_TC2.7.Rmd', eval = "TC2.7" %in% raw_data$test}

```

```{r, child='Test_TC2.8.Rmd', eval = "TC2.8" %in% raw_data$test}

```

```{r, child='Test_TC2.9.Rmd', eval = "TC2.9" %in% raw_data$test}

```

```{r, child='Test_TC2.10.Rmd', eval = "TC2.10" %in% raw_data$test}

```

```{r, child='Test_TC2.11.Rmd', eval = "TC2.11" %in% raw_data$test}

```

```{r, child='Test_TC2.12.Rmd', eval = "TC2.12" %in% raw_data$test}

```

```{r, child='Test_TC2.13.Rmd', eval = "TC2.13" %in% raw_data$test}

```

```{r, child='Test_TC2.14.Rmd', eval = "TC2.14" %in% raw_data$test}

```

```{r, echo=FALSE, results='asis',eval = "TC2.13" %in% raw_data$test}
cat("## TC2.13: LOD")
actual_test <- "TC2.13"
```

```{r, child='Test_TC2.15.Rmd', eval = "TC2.13" %in% raw_data$test}

```


```{r, child='Test_TC2.16.Rmd', eval = "TC2.16" %in% raw_data$test}

```

```{r, echo=FALSE, results='asis',eval = "TC3.5" %in% raw_data$test}
cat("## TC3.5: LOD")
actual_test <- "TC3.5"
```

```{r TC3.5, child='Test_TC4.3.Rmd', eval = "TC3.5" %in% raw_data$test}

```

```{r, echo=FALSE, results='asis',eval = "TC3.5" %in% raw_data$test}
#cat("## TC2.13: Cross talk")
actual_test <- "TC3.5"
```


```{r, child='Test_TC2.15.Rmd', eval = "TC3.5" %in% raw_data$test}

```


```{r, child='Test_TC3.6.Rmd', eval = "TC3.6" %in% raw_data$test}

```

```{r, echo=FALSE, results='asis',eval = "TC4.3" %in% raw_data$test}
cat("## TC4.3: LOD")
actual_test <- "TC4.3"
```

```{r TC4.3, child='Test_TC4.3.Rmd', eval = "TC4.3" %in% raw_data$test}

```

```{r, echo=FALSE, results='asis',eval = "TC5.3" %in% raw_data$test}
cat("## TC5.3: LOD")
actual_test <- "TC5.3"
```

```{r TC5.3, child='Test_TC4.3.Rmd', eval = "TC5.3" %in% raw_data$test}

```

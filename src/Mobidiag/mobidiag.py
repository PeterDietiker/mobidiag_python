import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import re
from glob import glob
import os
from plotnine import *
import subprocess
import shutil
import itertools
from pathlib import Path

class Mobidiag():
    '''
    Class to analyze and plot data from the Mobidiag Project. 
    '''
    
    def __init__(self,measurement = None,setup = "V3",Q705isTemp = True):
        '''
        Parameters
        ---------
        measurement: Measurement file
        type measurement: str or Paht instance
        setup: Setup which was used to acquire data
            "lab": old V2 setup at Volpi with decimal = ","
            "mobidiag": Data from Mobidiag for Labmodel V2 with decimal = "."
            "mobidiag_new" : Data from Mobidiag for Labmodel V2 with decimal = ","
            "mobidiag_paris": Data from Mobidiag Paris for Labmodel V2 with decimal = "."
            "lab_new" : Data from Volpi for Labmodel V2 with trigger data included
            "V2.5": Data from Volpi for Labmodel V3 with 4-quadrant chips
            "V3": Data from Volpi for Labmodel V3 with individual detector chips
        Q705isTemp: For old V2 setup: if true, channel Q705 is connected to temperatrue sensor. This parameter is depreciated

        
        
        '''
        self.measurement = Path(measurement)
        self.setup = setup
        self.Q705isTemp = Q705isTemp
        
        self.data = self.load_file(self.measurement, self.setup, self.Q705isTemp)




    def parse_header(self, measurement):
        '''
        Parses the header lines in the file (the oneplot_s starting with #) and saves the information i nthe paramters dict
        for every line not starting with # it adds a row to the self.header_df with the information in the parameters dict
        
        Paramters
        --------
        measurement: file name to parse
        
        Return
        -------
        Pandas datafram with header informations
        '''
        self.header_df = pd.DataFrame()
        parameters = {}
        with open(measurement, "r") as f:
            line = f.readline()
            line_index =0
            while line:
                if len(line)>2: #filter empty lines
                    if not line.startswith("#"):
                        self.header_df = self.header_df.append(pd.DataFrame(parameters,index=[line_index]))
                        line_index +=1
                    else:
                        key, value = line[1:].split(":")
                        parameters[str.strip(key)] = [str.strip(value)]
                line=f.readline()


    def load_file(self, measurement, setup = "lab", Q705isTemp = False):
        '''
        parses the header and loads the a csv into a pandas dataframe.
        It concatenates the header dataframe with the data datafarme and returns a new dataframe.
        
        Parameters:
        ----------
        measurement: file name to parse
        setup: setup used to generate data:
            "lab": old V2 setup at Volpi with decimal = ","
            "mobidiag": Data from Mobidiag for Labmodel V2 with decimal = "."
            "mobidiag_new" : Data from Mobidiag for Labmodel V2 with decimal = ","
            "mobidiag_paris": Data from Mobidiag Paris for Labmodel V2 with decimal = "."
            "lab_new" : Data from Volpi for Labmodel V2 with trigger data included
            "V3": Data from Volpi for Labmodel V2
        
        Q705isTemp: True if Q705 is temperature channel
        
        Return
        ---------
        Pandas dataframe
        
        '''
        
        self.parse_header(measurement)
        channel_names = ["a","b","c"]
        
        #data format depends on the used setup this is for V2:
        self.wavelength_dict = {"FAM":475,"HEX":525,"TAMRA":547,"ROX":580,"TYE665":631,"Q705":663}
        self.led_names = ["DARK","FAM","HEX","TAMRA","ROX","TYE665","Q705","SYBRGreen"]
        self.dye_names = ["FAM","TYE665","TAMRA","Q705","HEX","ROX","ATTO490LS","SYBRGreen"]
        self.dyes_without_atto = self.dye_names.copy()
        self.dyes_without_atto.remove("ATTO490LS")
        try:
            if setup == "lab":
                dateparse = lambda x: pd.datetime.strptime(x, '%d/%m/%Y %H:%M:%S,%f')
                columns_to_use = ["Date","Time","LED","Avrg. CH1","S.D. CH1","Avrg. CH2","S.D. CH2","Avrg. CH3","S.D. CH3","Avrg. CH4","S.D. CH4","Avrg. CH5","S.D. CH5","Avrg. CH6","S.D. CH6","Avrg. CH7","S.D. CH7","Avrg. CH8","S.D. CH8","Avrg. CH9","S.D. CH9","Avrg. CH10","S.D. CH10","Avrg. CH11","S.D. CH11","Avrg. CH12","S.D. CH12","Avrg. CH13","S.D. CH13","Avrg. CH14","S.D. CH14","Avrg. CH15","S.D. CH15","Avrg. CH16","S.D. CH16"]
                columns_all = columns_to_use.copy()
                #columns_all.insert(3,"deleteme_a")
                #columns_all.insert(4,"deleteme_b")
                #columns_all.append("deleteme_c")
                
                dataFrame = pd.read_csv(self.measurement, header = None,sep="\t",names = columns_all,usecols = columns_to_use,
                                        parse_dates={'datetime': ['Date', 'Time']}, date_parser=dateparse, comment ="#",decimal=b',')
                                        
                if self.Q705isTemp:
                    self.temp_channel = 4
                else:
                    self.temp_channel = None
                
            elif setup == "mobidiag":
                dateparse = lambda x: pd.datetime.strptime(x, '%d/%m/%Y %H:%M:%S.%f')
                columns_to_use = ["Date","Time","LED","Avrg. CH1","S.D. CH1","Avrg. CH2","S.D. CH2","Avrg. CH3","S.D. CH3","Avrg. CH4","S.D. CH4","Avrg. CH5","S.D. CH5","Avrg. CH6","S.D. CH6","Avrg. CH7","S.D. CH7","Avrg. CH8","S.D. CH8","Avrg. CH9","S.D. CH9","Avrg. CH10","S.D. CH10","Avrg. CH11","S.D. CH11","Avrg. CH12","S.D. CH12","Avrg. CH13","S.D. CH13","Avrg. CH14","S.D. CH14","Avrg. CH15","S.D. CH15","Avrg. CH16","S.D. CH16"]
                columns_all = columns_to_use.copy()
                columns_all.insert(3,"deleteme_a")
                columns_all.insert(4,"deleteme_b")
                columns_all.append("deleteme_c")
                
                dataFrame = pd.read_csv(self.measurement, header = None,sep=" ",names = columns_all,usecols = columns_to_use,
                                        parse_dates={'datetime': ['Date', 'Time']}, date_parser=dateparse, comment ="#",decimal=b'.')
                
                if self.Q705isTemp:
                    self.temp_channel = 4
                else:
                    self.temp_channel = None
                                        
            elif setup == "mobidiag_new":
                dateparse = lambda x: pd.datetime.strptime(x, '%d/%m/%Y %H:%M:%S,%f')
                columns_to_use = ["Date","Time","LED","Avrg. CH1","S.D. CH1","Avrg. CH2","S.D. CH2","Avrg. CH3","S.D. CH3","Avrg. CH4","S.D. CH4","Avrg. CH5","S.D. CH5","Avrg. CH6","S.D. CH6","Avrg. CH7","S.D. CH7","Avrg. CH8","S.D. CH8","Avrg. CH9","S.D. CH9","Avrg. CH10","S.D. CH10","Avrg. CH11","S.D. CH11","Avrg. CH12","S.D. CH12","Avrg. CH13","S.D. CH13","Avrg. CH14","S.D. CH14","Avrg. CH15","S.D. CH15","Avrg. CH16","S.D. CH16"]
                columns_all = columns_to_use.copy()
                columns_all.insert(3,"deleteme_a")
                columns_all.insert(4,"deleteme_b")
                print(len(columns_all))
                #columns_all.append("deleteme_c") #commented out 20.8.2020. data formet hast changed!?
                dataFrame = pd.read_csv(self.measurement, header = None,sep="\t", comment ="#",decimal=b'.')
                print(len(dataFrame.columns))
                dataFrame = pd.read_csv(self.measurement, header = None,sep="\t",names = columns_all,usecols = columns_to_use,
                                        parse_dates={'datetime': ['Date', 'Time']}, date_parser=dateparse, comment ="#",decimal=b',')
                
                if self.Q705isTemp:
                    self.temp_channel = 4
                else:
                    self.temp_channel = None
            
            elif setup == "mobidiag_paris":
                dateparse = lambda x: pd.datetime.strptime(x, '%d/%m/%Y %H:%M:%S.%f')
                columns_to_use = ["Date","Time","LED","Avrg. CH1","S.D. CH1","Avrg. CH2","S.D. CH2","Avrg. CH3","S.D. CH3","Avrg. CH4","S.D. CH4","Avrg. CH5","S.D. CH5","Avrg. CH6","S.D. CH6","Avrg. CH7","S.D. CH7","Avrg. CH8","S.D. CH8","Avrg. CH9","S.D. CH9","Avrg. CH10","S.D. CH10","Avrg. CH11","S.D. CH11","Avrg. CH12","S.D. CH12","Avrg. CH13","S.D. CH13","Avrg. CH14","S.D. CH14","Avrg. CH15","S.D. CH15","Avrg. CH16","S.D. CH16"]
                columns_all = columns_to_use.copy()
                columns_all.insert(3,"deleteme_a")
                columns_all.insert(4,"deleteme_b")
                #columns_all.append("deleteme_c")
                
                dataFrame = pd.read_csv(self.measurement, header = None,sep="\t",names = columns_all,usecols = columns_to_use,
                                        parse_dates={'datetime': ['Date', 'Time']}, date_parser=dateparse, comment ="#",decimal=b'.')
                                        
                if self.Q705isTemp:
                    self.temp_channel = 4
                else:
                    self.temp_channel = None
            
            elif setup == "lab_new":
                dateparse = lambda x: pd.datetime.strptime(x, '%d/%m/%Y %H:%M:%S.%f')
                columns_to_use = ["Date","Time","LED","Avrg. CH1","S.D. CH1","Avrg. CH2","S.D. CH2","Avrg. CH3","S.D. CH3","Avrg. CH4","S.D. CH4","Avrg. CH5","S.D. CH5","Avrg. CH6","S.D. CH6","Avrg. CH7","S.D. CH7","Avrg. CH8","S.D. CH8","Avrg. CH9","S.D. CH9","Avrg. CH10","S.D. CH10","Avrg. CH11","S.D. CH11","Avrg. CH12","S.D. CH12","Avrg. CH13","S.D. CH13","Avrg. CH14","S.D. CH14","Avrg. CH15","S.D. CH15","Avrg. CH16","S.D. CH16"]
                columns_all = columns_to_use.copy()
                #columns_all.insert(3,"deleteme_a")
                #columns_all.insert(4,"deleteme_b")
                #columns_all.append("deleteme_c")
                
                dataFrame = pd.read_csv(self.measurement, header = None,sep="\t",names = columns_all,usecols = columns_to_use,
                                        parse_dates={'datetime': ['Date', 'Time']}, date_parser=dateparse, comment ="#",decimal=b'.')
                
                if self.Q705isTemp:
                    self.temp_channel = 4
                else:
                    self.temp_channel = None
            
            elif setup in ["V2.5","V3"]:
                
                self.wavelength_dict = {"FAM":465,"HEX":525,"ALEXA750":720,"ROX":570,"Q670":625,"Q705":685}
                self.led_names = ["DARK","FAM","HEX","ROX","Q670","Q705","ALEXA750","SYBRGreen"]
                self.dye_names = ["FAM","Q670","ALEXA750","Q705","HEX","ROX","ATTO490LS","SYBRGreen"]
                self.dyes_without_atto = self.dye_names.copy()
                self.dyes_without_atto.remove("ATTO490LS")
                
                dateparse = lambda x: pd.datetime.strptime(x, '%d/%m/%Y %H:%M:%S,%f')
                columns_to_use = ["Date","Time","LED","Avrg. CH1","S.D. CH1","Avrg. CH2","S.D. CH2","Avrg. CH3","S.D. CH3","Avrg. CH4","S.D. CH4","Avrg. CH5","S.D. CH5","Avrg. CH6","S.D. CH6","Avrg. CH7","S.D. CH7","Avrg. CH8","S.D. CH8","Avrg. CH9","S.D. CH9","Avrg. CH10","S.D. CH10","Avrg. CH11","S.D. CH11","Avrg. CH12","S.D. CH12","Avrg. CH13","S.D. CH13","Avrg. CH14","S.D. CH14","Avrg. CH15","S.D. CH15","Avrg. CH16","S.D. CH16"]
                columns_all = columns_to_use.copy()
                #columns_all.insert(3,"deleteme_a")
                #columns_all.insert(4,"deleteme_b")
                #columns_all.append("deleteme_c")
                
                dataFrame = pd.read_csv(self.measurement, header = None,sep="\t",names = columns_all,usecols = columns_to_use,
                                        parse_dates={'datetime': ['Date', 'Time']}, date_parser=dateparse, comment ="#",decimal=b',')
                
                if setup == "V2.5":
                     self.temp_channel = 10
                
                if setup == "V3":
                     self.temp_channel = 1
                
            
                
            else:
                print("select correct setup: 'lab', 'mobidiag','V2.5','V3'")
                return
        except (pd.errors.ParserError, TypeError):
            print("wrong setup selected. Please try different setup")
            raise
            return
        #rename channels from number to Dye
        
        if setup == "V2.5":
            channel_names = ["Avrg. CH1","S.D. CH1","Avrg. CH2","S.D. CH2","Avrg. CH3","S.D. CH3","Avrg. CH4","S.D. CH4","Avrg. CH5","S.D. CH5","Avrg. CH6","S.D. CH6","Avrg. CH7","S.D. CH7","Avrg. CH8","S.D. CH8","Avrg. CH9","S.D. CH9","Avrg. CH10","S.D. CH10","Avrg. CH11","S.D. CH11","Avrg. CH12","S.D. CH12","Avrg. CH13","S.D. CH13","Avrg. CH14","S.D. CH14","Avrg. CH15","S.D. CH15","Avrg. CH16","S.D. CH16"]
            #filter_names = ["FAM","Q670","ALEXA750","Q705_a","Q705_b","HEX_a","ROX","HEX_b"]
            #filter order has changed for V2.5 at some point. See e-mail from Mikolaj Glinksi from 18.08.2021
            filter_names = ["FAM","Q705_a","ROX","Q705_b","HEX_a","ALEXA750","Q670","HEX_b"]
        elif setup == "V3":
            channel_names = ["Avrg. CH2","S.D. CH2","Avrg. CH3","S.D. CH3","Avrg. CH4","S.D. CH4","Avrg. CH5","S.D. CH5","Avrg. CH6","S.D. CH6","Avrg. CH7","S.D. CH7","Avrg. CH8","S.D. CH8","Avrg. CH9","S.D. CH9","Avrg. CH10","S.D. CH10","Avrg. CH11","S.D. CH11","Avrg. CH12","S.D. CH12","Avrg. CH13","S.D. CH13"]
            filter_names = ["FAM","HEX","ROX","Q670","Q705","ALEXA750"]
        else:
            channel_names = ["Avrg. CH1","S.D. CH1","Avrg. CH2","S.D. CH2","Avrg. CH3","S.D. CH3","Avrg. CH4","S.D. CH4","Avrg. CH5","S.D. CH5","Avrg. CH6","S.D. CH6","Avrg. CH7","S.D. CH7","Avrg. CH8","S.D. CH8","Avrg. CH9","S.D. CH9","Avrg. CH10","S.D. CH10","Avrg. CH11","S.D. CH11","Avrg. CH12","S.D. CH12","Avrg. CH13","S.D. CH13","Avrg. CH14","S.D. CH14","Avrg. CH15","S.D. CH15","Avrg. CH16","S.D. CH16"]
            filter_names = ["FAM","TYE665","TAMRA","Q705_a","Q705_b","HEX_a","ROX","HEX_b"]

        

        ############
        #rename temperature channel 
        ##############
        
        if self.temp_channel is not None:
            temp_channel_name = f"Avrg. CH{self.temp_channel}"
            temp_sd_name = f"S.D. CH{self.temp_channel}"
            temp_rename_dict = {temp_channel_name:"TEMP_LE_avg", temp_sd_name: "TEMP_LE_sd"}
            dataFrame = dataFrame.rename(columns = temp_rename_dict)
            
        #rename all other channels
        filter_names = [_filter+chamber+value for ((chamber, _filter),value) in itertools.product(itertools.product(["_1","_2"],filter_names),["_avg","_sd"])]
        columns_rename_dict = {channel:_filter for (channel,_filter) in zip(channel_names, filter_names)}
        dataFrame = dataFrame.rename(columns = columns_rename_dict)


        #rename LED entries from number to Dye
        
        
        led_rename_dict = {led:dye for led, dye in zip(range(8),self.led_names)}
        dataFrame["LED"] = dataFrame["LED"].map(led_rename_dict)
        
        
        n_leds = len(pd.unique(dataFrame['LED']))
        #add measurement variable
        dataFrame["measurement"] = np.repeat(np.arange(len(dataFrame)/n_leds),n_leds)
        
        
        #copy avg columns to _raw
        columns_avg = [col for col in filter_names if (("avg" in col) and ("TEMP" not in col))]
        if self.temp_channel is not None:
            try:
                columns_avg.remove(columns_rename_dict[temp_channel_name]) #temperature channel
            except KeyError:
                #in V3, this column was allready removed
                pass
        column_names_raw = [column.split("_avg")[0]+"_raw" for column in columns_avg]
        for col,col_raw in zip(columns_avg,column_names_raw):
            dataFrame[col_raw] = dataFrame[col]

        #correct for Dark value
        for i in range(len(dataFrame)):
            dark_row = i//n_leds*n_leds
            if i%n_leds > 0:
                dataFrame.loc[i,columns_avg] = dataFrame.loc[i,columns_avg] - dataFrame.loc[dark_row,columns_avg]
        
        
        for dye in self.dye_names:
            try:
                self.header_df[dye] = self.header_df[dye].astype(int)
            except:
                print("warning")
        
        dataFrame_concatenated = pd.concat([self.header_df,dataFrame],axis = 1)
        
        
        
        dataFrame_concatenated = self.add_missing_parameters(dataFrame_concatenated)
        
        #calculate temperatures
        try:
            dataFrame_concatenated['TEMP_LE_avg'] = dataFrame_concatenated.apply(lambda x: self.calculate_temperature(x['TEMP_LE_avg'],x['LABMODEL'],self.setup),axis=1)
        except KeyError:
            #temüperature "TEMP_LE_avg" not found
            pass
        return dataFrame_concatenated
    

    def add_missing_parameters(self, dataFrame):
        '''
        adds missing parameters
        '''
        if "TEMP" not in dataFrame.columns:
            dataFrame["TEMP"] = 0
        if "LABMODEL" not in dataFrame.columns:
            dataFrame["LABMODEL"] = "0"
        return dataFrame
    
    @staticmethod
    def calculate_temperature(rawData, labmodel = "0", setup = "V3"):
        '''
        calculates temperature from raw data
        
        It is used on Channel 4 which is not Quasar anymore but temperature reading using the NTC on the light engine. 
        Calibrated raw data is used to calculate temperature via resistance. 
        
        parameters:
        rawData: raw data to convert
        labmodel: labmodel. Is needed to set to calibration coefficients. Labmodel 200601 is used as default and fallback. 
        '''
        if labmodel == "0":
            #print("Labmodel not defined. Use default: 200601")
            labmodel = "200601"
        
        #coefficients = {"200601":[0.001518,3.29137E-5,6.51076E-17,2.89454E-19],
        #                    "200602":[0.071541,0.000164444,3.25291E-16,1.44617E-18],
        #                    "200603":[0.0647191,0.000164512,3.30172E-15,6.22893E-19],
        #                    "200604":[0.0688903,0.000164469,-7.77258E-15,1.68846E-18],
        #                    "200605":[0.0729653,0.000164402,2.96833E-15,9.83625E-19]}
        coefficients = {"200601":[0.0102991,3.29024E-5,-7.97948E-16,1.43066E-21],
                            "200602":[0.01518,3.29137E-5,6.51076E-17,2.89454E-19],
                            "200603":[0.0116934,3.29529E-5,6.61358E-16,1.2477E-19],
                            "200604":[0.0136901,3.29152E-5,-1.55553E-15,3.37911E-19],
                            "200605":[0.0143876,3.28897E-5,5.93832E-16,1.9678E-19]}
        if not labmodel in coefficients.keys():
            labmodel = "200601"
        if setup in ["V2.5","V3"]:
            inverted = -(rawData-2000)
            polynomial = np.poly1d(coefficients[labmodel][::-1])
            Rt = polynomial(inverted) * 270000 / (5-polynomial(inverted))
            temperature = (1/((1/298) +(1/3380)*np.log(Rt/10000)))-273.15
        else:
            inverted = -(rawData-2000)
            polynomial = np.poly1d(coefficients[labmodel][::-1])
            Rt = 1.64805/((5/(polynomial(inverted)*32768))-0.0000305194)
            temperature = (1/((1/298) +(1/3380)*np.log(Rt/10000)))-273.15
        return temperature
    

    def save_data(self,file_name):
        '''
        exports a Pandas dataframe as csv
        Parameters
        -----------
        dataframe: Pandas dataframe to export
        file_name: csv filename
        '''
        self.data.to_csv(file_name)


    def generate_R_script(self, data_file, title):
            '''
            Generates a R script which renders the report using R. 
            Unfortunately self cannot be done from Python directly. Thats why we have to modify an R script and run it afterwards.
            
            Parameters
            ----------
            data_file: Data file to use
            title: title of the report
            '''
            module_dir = Path(__file__)
            #print(module_dir.resolve())
            #print((module_dir/r"..\..\Mobidiag_report\Mobidiag_test_evaluation_knit_template.R").resolve())
            script_path = module_dir/r"..\..\Mobidiag_report\Mobidiag_test_evaluation_knit.R"
            R_script = shutil.copyfile(module_dir/r"..\..\Mobidiag_report\Mobidiag_test_evaluation_knit_template.R",module_dir/r"..\..\Mobidiag_report\Mobidiag_test_evaluation_knit.R")
            R_script = R_script.resolve()
            rmd_path = module_dir/r"..\..\Mobidiag_report\Mobidiag_test_evaluation.Rmd"
            with open(R_script, 'r') as file :
              file_text = file.read()

            output_file = os.path.join(os.path.dirname(self.measurement),title+".html")
            output_file = str(output_file).replace("\\","\\\\")
            data_file = str(data_file).replace("\\","\\\\")
            title = str(title).replace("\\","\\\\")
            rmd_path = str(rmd_path).replace("\\","\\\\")
            # Replace the target string
            file_text = file_text.replace("test file", data_file)
            file_text = file_text.replace("test title", title)
            file_text = file_text.replace("report file", output_file)
            file_text = file_text.replace("Mobidiag_test_evaluation.Rmd",rmd_path) 
            if self.setup in ["V2.5","V3"]:
                setup = self.setup
            else:
                setup = "V2"
            file_text = file_text.replace("test setup", setup)
            

            # Write the file out again
            with open(R_script, 'w') as file:
              file.write(file_text)
            return R_script

    def generate_report(self, title = None):
        '''
        generates the report of data_file
        Parameters
        ---------
        title: Title of the report. If title is None, file name of data_file is used
        '''
        if title is None:
            title = os.path.splitext(self.measurement)[0]
            title = str(title).replace("\\","\\\\")
        csv_file_name = os.path.splitext(self.measurement)[0]+"_converted.csv"
        self.save_data(csv_file_name)
        R_script = self.generate_R_script(csv_file_name, title)
        subprocess.check_output(["Rscript", str(R_script)], shell= True)
        os.remove(R_script)


    def sort_by_wavelength(self, df):
        df["wavelength"] = df['Dye'].apply(lambda x: self.wavelength_dict[x.split("_")[0]] if x.split("_")[0] in self.wavelength_dict else 0 )
        df = df.sort_values(["wavelength","Dye"])
        df = df.assign(Dye = pd.Categorical(df.Dye, pd.unique(df.Dye),ordered=True))
        df["Dye"] = df["Dye"].astype('category')
        return df


    def plot_counts(self, values = "avg", stats = "mean",concentration="all", led = 'own',scale ='lin', group_factors = ["TEC1"],return_data = False):
        '''
        Plots the ADC counts for each channel
        Parameters:
        ----------
        values: what data to plot:
            "avg": in-sample averages corrected for dark
            "sd": in-sample standard deviations
            "raw": in samples average not corrected
        stats: statistics to use to aggregate the data for different samples:
            "mean": take mean value
            "std": calculate sigma
        concentration: concentrations to plot in nM:
            "all": plot all concentrations
            "blank": plot only blank measurements
            "attoblank": plot only blank with Atto490SL measurements
            250 : plot a particular concentration
        led: Selection of LED:
            "all": Plot data of all leds
            "own": Plot data of led corresponding to the channel
            "HEX": select a particular led
            "DARK": select Dark values
        scale: select scale of y axis:
            "lin": linear scale
            "log": logarithmic scale
        group_factors = addidional factors to group data
        '''
        group_factors = list(set(group_factors + self.dye_names + ["LED"]))
        group_factors = [col for col in group_factors if col in self.data.columns]
        if stats == "std":
            grouped = self.data.groupby(group_factors).std()[self.data.columns[[values in x for x in self.data.columns]]]
        else:
            grouped = self.data.groupby(group_factors).mean()[self.data.columns[[values in x for x in self.data.columns]]]
        
        df_long = grouped.reset_index().melt(group_factors,var_name="Dye",value_name = "ADC_Count")

        long_filtered = df_long.copy()
        #get rid of TEMP column
        long_filtered = long_filtered[long_filtered["Dye"]!= "TEMP_LE_avg"]
        faceting = False
        if led == "own":
            LED_filter_list = [x in y for x,y in zip(long_filtered["LED"],long_filtered["Dye"])]
            long_filtered = long_filtered[LED_filter_list]
        elif led == "all":
            #do not filter
            faceting = True
        elif led in (self.dye_names+["DARK"]):
            LED_filter_list = long_filtered["LED"] == led
            long_filtered = long_filtered[LED_filter_list]
        else:
            print("wrong led selected: "+led)
            return
        
        if concentration =="all":
            #do not filter
            pass
        elif concentration == "blank":
            conc_available = [dye for dye in self.dye_names if dye in self.data.columns]
            conc_filter_list = long_filtered.loc[:,conc_available].sum(axis=1)==0
            long_filtered = long_filtered[conc_filter_list]
            
        elif concentration == "attoblank":
            conc_available = [dye for dye in self.dyes_without_atto if dye in self.data.columns]
            conc_filter_list = long_filtered.loc[:,conc_available].sum(axis=1)==0
            long_filtered = long_filtered[conc_filter_list]
            long_filtered = long_filtered[long_filtered["ATTO490LS"]>0]
            
        else:
            #return long_filtered
            conc_filter_list = [int(long_filtered.loc[idx,conc])==concentration for idx,conc in zip(long_filtered.index,long_filtered["Dye"].apply(lambda x:x.split("_")[0]))]
            long_filtered = long_filtered[conc_filter_list]

        long_filtered = self.sort_by_wavelength(long_filtered)
        
        if values == "avg":
            ylabel = "ADC_Count"
            title = "Blank corrected ADC count per channel"
        if values == "sd":
            ylabel = "Sigma of ADC_Count"
            title = "Sigma of ADC count per channel"
        if values == "raw":
            ylabel = "ADC_Count"
            title = "Raw ADC count per channel"
          
        try:
            if not faceting:
                if "TEC1" in self.data.columns:
                    plot_object = ggplot(long_filtered)+geom_point(aes("Dye","ADC_Count",group = "TEC1",color="TEC1"))
                else:
                    plot_object = ggplot(long_filtered)+geom_point(aes("Dye","ADC_Count"))
                plot_object = plot_object+theme(axis_text_x=element_text(rotation=90, hjust=1),subplots_adjust={'bottom':0.2})+\
                ylab(ylabel)
            else:
                if "TEC1" in self.data.columns:
                    plot_object = ggplot(long_filtered)+geom_point(aes("Dye","ADC_Count",group = "TEC1",color="TEC1"))
                else:
                    plot_object = ggplot(long_filtered)+geom_point(aes("Dye","ADC_Count"))
                plot_object = plot_object+theme(axis_text_x=element_text(rotation=90, hjust=1),subplots_adjust={'right':0.8,'bottom':0.2},figure_size = (8,10))
                plot_object = plot_object + facet_grid("LED~",scales = "free_y")
            if scale == "log":
                limits =np.clip([long_filtered["ADC_Count"].min()/2,long_filtered["ADC_Count"].max()*2],1,1E6)
                plot_object = plot_object+scale_y_log10(limits=limits)
            else:
                limits =[-10,long_filtered["ADC_Count"].max()*1.1]
                plot_object = plot_object+ylim(limits)
            
            plot_object = plot_object + theme(subplots_adjust={'bottom': 0.3,"right":0.85})
        except AttributeError:
            print("Wrong concentration selected")
            return
        if return_data:
            return long_filtered, plot_object
        else:
            plot_object.draw()
            return plot_object

    def plot_slope(self, blank = "blank",return_data = False):
        '''
        Calculates the slope for each channel and plots it
        Parameters:
        ----------
        "blank": Which measurement to use as blank
            "blank": Real blank, all concentrations zero
            "attoblank": All concentrations except Atto490SL zero
        '''
        
        
        background, fig = self.plot_counts(led="own",values = "avg",stats = "mean",concentration=blank, return_data=True)
        max_conc, fig = self.plot_counts(led="own",values = "avg",stats = "mean",concentration=250, return_data= True)
        all_data = pd.concat([background,max_conc],axis = 0)
        #cannot reset_index with categorical columns. see https://github.com/pandas-dev/pandas/issues/19136
        all_data['Dye'] = all_data['Dye'].astype(str)
        all_data_wide = all_data.pivot_table(values="ADC_Count", index=['TEC1']+self.dyes_without_atto , columns='Dye')
        '''
        Abzug der background resp. buffer messungen.  
        Diese ist jeweils die erste messung pro TEC. Daher kann einfach die erste in jedem set von 7 Messungen abgezogen werden.
        '''
        for i in range(len(all_data_wide)):
            background_row = i//7*7
            if i%7 > 0:
                all_data_wide.iloc[i] = all_data_wide.iloc[i] - all_data_wide.iloc[background_row]
        all_data_long = all_data_wide.reset_index().melt(["TEC1"]+self.dyes_without_atto,var_name="Dye",value_name = "slope")
        #regenerate categorical Dye column
        all_data_long = self.sort_by_wavelength(all_data_long)
        all_data_long_filtered = all_data_long[all_data_long.loc[:,self.dyes_without_atto].sum(axis=1)>0]
        all_data_long_filtered = all_data_long_filtered[all_data_long_filtered["slope"].notna()]
        all_data_long_filtered["slope"] = all_data_long_filtered["slope"]/250
        
        all_data_long_filtered = self.sort_by_wavelength(all_data_long_filtered)
        
        plot_object = ggplot(all_data_long_filtered)+geom_point(aes("Dye","slope",group = "TEC1",color="TEC1"))+\
        theme(axis_text_x=element_text(rotation=90, hjust=1),subplots_adjust={'bottom':0.2})+ylab("slope / count / nM")
        if return_data:
            return all_data_long_filtered, plot_object
        else:
            plot_object.draw()
            return plot_object




    def plot_lod(self, blank = "blank", SD_measurement = 50,  return_data = False, limits = None):
        '''
        calculates slope and LOD and plots it
        Parameters:
        ----------
        "blank": Which measurement to use as blank for the calculation of the slope
            "blank": Real blank, all concentrations zero
            "attoblank": All concentrations except Atto490SL zero
        SD_measurement: Which measurement to use to calculate SD
            "blank": Real blank, all concentrations zero
            "attoblank": All concentrations except Atto490SL zero
            concentration: any concentration
        '''
        
        #
        data,fig = self.plot_slope(blank = blank, return_data=True)
        slope_grouped = data.groupby(["TEC1","Dye"])
        std, fig  = self.plot_counts(stats = "std", concentration=SD_measurement, return_data=True) 
        std_grouped = std.groupby(["TEC1","Dye"])
        lod = 3*std_grouped.mean()["ADC_Count"] / slope_grouped.mean()["slope"]
        lod = pd.concat([std_grouped.mean(),slope_grouped.mean(),lod],axis=1)
        lod = lod.rename({"ADC_Count":"SD","slope":"Slope",0:"LOD"},axis=1)
        #lod = lod.drop( "wavelength", axis = 1) #get rid of wavelength column
        lod = lod[["SD","LOD","Slope"]]
        lod_long = lod.reset_index().melt(["TEC1","Dye"],var_name="type",value_name = "value")
        
        lod_long = self.sort_by_wavelength(lod_long)

        y_limits =np.clip([lod_long["value"].min()/2,lod_long["value"].max()*2],1E-1,1E6)
        #lod_long = lod_long[~lod_long['Dye'].isin(["Q705_b_1_avg","Q705_b_2_avg"])]
        plot_object = ggplot(lod_long)+geom_point(aes("Dye","value",group = "TEC1",color="TEC1"))+\
        facet_grid("type~",scales="free_y")+\
        theme(axis_text_x=element_text(rotation=90, hjust=1),subplots_adjust={'bottom':0.3,'right':0.8})+\
        labs(y=" counts / nM        counts           nM")
        
        if limits is not None:
            plot_object = plot_object+scale_y_log10(limits = limits)
        else:
            plot_object = plot_object+scale_y_log10(limits = y_limits)
        
        #scale_y_log10(limits = y_limits)+\
        
        if return_data:
            return lod_long, plot_object
        else:
            plot_object.draw()
            return plot_object

    def get_time_series(self,concentration = "all",types = "avg",led = 'own',temp = False):
        '''
        returns the dataframe in long format ready for plotting the channel-wise time series
        Paramters
        ---------
        df: pandas data frame
        concentration: concentration to use. 0 means buffer
        types : type of data to use:
            "avg": average of ADC reads
            "sd": standard deviation of ADC reads
            "cv": relative standard deviations in percent
            "raw": raw, not dark corrected values
            "temp" get light engine temperature
        led: Selection of LED:
            "own": get data of led corresponding to the channel
            "HEX": get data of a particular led
            "DARK": get Dark values
        '''
        
        if temp == True:
            types = "TEMP_LE_"+types
            led = "DARK"

            
        #self.dye_names = ["FAM","TYE665","TAMRA","Q705","HEX","ROX"]
        all_parameters = [col for col in self.data.columns if not (("_sd" in col) or ("_avg" in col) or ("_raw" in col))]
        all_parameters_without_measurement = all_parameters.copy()
        all_parameters_without_measurement.remove("measurement")
        all_parameters_without_measurement.remove('datetime')

        if types == "cv":
            data_column = "sd"
        else:
            data_column  = types

        grouped_mean = self.data.groupby(all_parameters_without_measurement).mean()[self.data.columns[[types in x for x in self.data.columns]]]
        #find out how many measurements we took under same conditions
        #self only works if we have the same number of measurements under all conditions
        nr_mesurements = len(self.data)/len(grouped_mean)
        nr_measurements_array = np.repeat(np.arange(nr_mesurements),len(pd.unique(self.data.LED)))
        nr_measurements_array = np.tile(nr_measurements_array,int(len(self.data)/nr_measurements_array.shape[0]))
        #self.data['measurement'] = nr_measurements_array[:len(self.data)]
        if types == "cv":
            grouped_all_avg = self.data.groupby(all_parameters).mean()[self.data.columns[["avg" in x for x in self.data.columns]]]
            rename_dict = {col:col.split("_avg")[0]+"_cv" for col in grouped_all_avg.columns}
            grouped_all_avg = grouped_all_avg.rename(columns = rename_dict)
            grouped_all_sd = self.data.groupby(all_parameters).mean()[self.data.columns[["sd" in x for x in self.data.columns]]]
            rename_dict = {col:col.split("_sd")[0]+"_cv" for col in grouped_all_sd.columns}
            grouped_all_sd = grouped_all_sd.rename(columns = rename_dict)
            grouped_all = grouped_all_sd / grouped_all_avg
            grouped_avg = self.data.groupby(all_parameters_without_measurement).mean()[self.data.columns[["avg" in x for x in self.data.columns]]]
            rename_dict = {col:col.split("_avg")[0]+"_cv" for col in grouped_avg.columns}
            grouped_avg = grouped_avg.rename(columns = rename_dict)
            grouped_sd = self.data.groupby(all_parameters_without_measurement).mean()[self.data.columns[["sd" in x for x in self.data.columns]]]
            rename_dict = {col:col.split("_sd")[0]+"_cv" for col in grouped_sd.columns}
            grouped_sd = grouped_sd.rename(columns = rename_dict)
            grouped_mean = grouped_sd / grouped_avg
            #return grouped_mean
        else:
            grouped_all = self.data.groupby(all_parameters).mean()[self.data.columns[[types in x for x in self.data.columns]]]
        grouped_absolute = grouped_all
        grouped_mean_substracted = grouped_all - grouped_mean
        grouped_relative = grouped_all / grouped_mean
        if types == "cv":
            #cv in percent
            grouped_absolute = grouped_absolute*100
            grouped_mean_substracted = grouped_mean_substracted*100
            grouped_relative = grouped_relative*100
        long_all_absolute = grouped_absolute.reset_index().melt(all_parameters,var_name="Dye",value_name = "ADC_Count")
        long_all_mean_substracted = grouped_mean_substracted.reset_index().melt(all_parameters,var_name="Dye",value_name = "ADC_Count")
        long_all_relative = grouped_relative.reset_index().melt(all_parameters,var_name="Dye",value_name = "ADC_Count")
        if led == 'own':
            LED_filter_list_abs = [x in y for x,y in zip(long_all_absolute["LED"],long_all_absolute["Dye"])]
            LED_filter_list_sub = [x in y for x,y in zip(long_all_mean_substracted["LED"],long_all_mean_substracted["Dye"])]
            LED_filter_list_rel = [x in y for x,y in zip(long_all_relative["LED"],long_all_relative["Dye"])]
        elif led in (self.dye_names+["DARK"]):
            LED_filter_list_abs = long_all_absolute["LED"] == led
            LED_filter_list_sub = long_all_mean_substracted["LED"] == led
            LED_filter_list_rel = long_all_relative["LED"] == led
        else:
            print("wrong led selected: "+led)
        #LED_filter_list = long_all_absolute["LED"] == "ROX"
        long_all_absolute = long_all_absolute[LED_filter_list_abs]
        long_all_mean_substracted = long_all_mean_substracted[LED_filter_list_sub]
        long_all_relative = long_all_relative[LED_filter_list_rel]
        
        if concentration =="all":
            #do not filter
            pass
        elif concentration == "blank":
            conc_filter_list = long_all_absolute.loc[:,self.dyes_without_atto].sum(axis=1)==0
            long_all_absolute = long_all_absolute[conc_filter_list]
            conc_filter_list = long_all_mean_substracted.loc[:,self.dyes_without_atto].sum(axis=1)==0
            long_all_mean_substracted = long_all_mean_substracted[conc_filter_list]
            conc_filter_list = long_all_relative.loc[:,self.dyes_without_atto].sum(axis=1)==0
            long_all_relative = long_all_relative[conc_filter_list]
        else:
            conc_filter_list = [int(long_all_absolute.loc[idx,conc])==concentration for idx,conc in zip(long_all_absolute.index,long_all_absolute["Dye"].apply(lambda x:x.split("_")[0]))]
            long_all_absolute = long_all_absolute[conc_filter_list]
            conc_filter_list = [int(long_all_mean_substracted.loc[idx,conc])==concentration for idx,conc in zip(long_all_mean_substracted.index,long_all_mean_substracted["Dye"].apply(lambda x:x.split("_")[0]))]
            long_all_mean_substracted = long_all_mean_substracted[conc_filter_list]
            conc_filter_list = [int(long_all_relative.loc[idx,conc])==concentration for idx,conc in zip(long_all_relative.index,long_all_relative["Dye"].apply(lambda x:x.split("_")[0]))]
            long_all_relative = long_all_relative[conc_filter_list]

        
        long_all_absolute = long_all_absolute.rename(columns = {"ADC_Count":"absolute"})
        long_all_mean_substracted = long_all_mean_substracted.rename(columns = {"ADC_Count":"deviation"})
        long_all_relative = long_all_relative.rename(columns = {"ADC_Count":"rel. deviation"})
        

        import functools 
        all_data = functools.reduce(lambda a,b : pd.merge(a,b),[long_all_absolute,long_all_mean_substracted,long_all_relative])

        #return long_all_absolute,long_all_mean_substracted,long_all_relative,all_data
        if not temp:
            all_data = self.sort_by_wavelength(all_data)
        return all_data.dropna()

    def plot_time_series(self,concentration = "all",types = "avg",led = 'own',values = "absolute", facet=None):
        '''
        Plots the time series channel-wise
        parameters:
        ----------
        concentration: concentration to use. Use "blank" fur blank
        types : type of data to use:
            "avg": average of ADC reads
            "sd": standard deviation of ADC reads
            "cv": relative standard deviations in percent
            "raw": raw, not dark corrected values
            "temp" get light engine temperature
        led: Selection of LED:
            "own": get data of led corresponding to the channel
            "HEX": get data of a particular led
            "DARK": get Dark values
            
        values: which values to use:
                "absolute" : plot absolute values
                "deviation": plot deviation from mean value of the time series
                "rel. deviation" : plot relative deviation
        facet:  Specification for faceting: e.g. "Dye ~ TEC1+TEMP"
                
        '''
        ylabel = ""
        title = ""
        if values == "absolute":
            ylabel = "counts"
            title = "time series of absolute values"
        elif values == "deviation":
            ylabel = "mean substracted counts"
            title = "time series of mean substracted value"
        elif values == "rel. deviation":
            ylabel = "rel. deviation from mean"
            title = "time series of rel. deviations"
        
        times_series = self.get_time_series(concentration, types, led)
        
        plot_object = ggplot(times_series)+geom_point(aes("measurement",values))+geom_line(aes("measurement",values))+\
            theme(axis_text_x=element_text(rotation=90, hjust=1),subplots_adjust={'right':0.8,'bottom':0.2},figure_size = (8,12))+\
            theme(strip_text_y = element_text(angle = 0,margin={"l":45}))+labs(y=ylabel,title = title)
        if facet is not None:
            plot_object = plot_object + facet_grid(facet,scales = "free_y",labeller = "label_both")
        plot_object.draw()
        return plot_object
